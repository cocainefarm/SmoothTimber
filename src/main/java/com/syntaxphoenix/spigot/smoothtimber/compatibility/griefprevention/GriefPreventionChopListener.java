package com.syntaxphoenix.spigot.smoothtimber.compatibility.griefprevention;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.syntaxphoenix.spigot.smoothtimber.event.AsyncPlayerChopTreeEvent;
import com.syntaxphoenix.spigot.smoothtimber.event.reason.DefaultReason;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.ClaimPermission;
import me.ryanhamshire.GriefPrevention.DataStore;
import me.ryanhamshire.GriefPrevention.GriefPrevention;

public final class GriefPreventionChopListener implements Listener {

	protected GriefPreventionChopListener() {

	}

	private final DataStore store = GriefPrevention.instance.dataStore;

	@EventHandler(ignoreCancelled = true)
	public void onChopEvent(AsyncPlayerChopTreeEvent event) {
		UUID uniqueId = event.getPlayer().getUniqueId();
		if (store.getPlayerData(uniqueId).ignoreClaims) {
			return;
		}
		for (Location location : event.getBlockLocations()) {
			Claim claim = store.getClaimAt(location, false, null);
			if (claim == null) {
				continue;
			}
			if (!claim.hasExplicitPermission(uniqueId, ClaimPermission.Build)) {
				event.setCancelled(true);
				event.setReason(DefaultReason.GRIEFPREVENTION);
				return;
			}
		}
	}
}