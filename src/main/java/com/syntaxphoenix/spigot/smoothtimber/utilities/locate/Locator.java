package com.syntaxphoenix.spigot.smoothtimber.utilities.locate;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import org.bukkit.Location;
import org.bukkit.block.Block;

import com.syntaxphoenix.spigot.smoothtimber.config.config.CutterConfig;
import com.syntaxphoenix.spigot.smoothtimber.utilities.PluginUtils;
import com.syntaxphoenix.spigot.smoothtimber.utilities.limit.IntCounter;

public abstract class Locator {

	private static Function<Location, Block> BLOCK_DETECTOR;
	private static LocationResolver LOCATION_RESOLVER = DefaultResolver.INSTANCE;

	public static void setSyncBlockDetection(boolean sync) {
		BLOCK_DETECTOR = sync ? (location) -> {
			try {
				return PluginUtils.getObjectFromMainThread(() -> location.getBlock());
			} catch (Exception ignore) {
				return null;
			}
		} : (location) -> {
			try {
				return Objects.requireNonNull(location.getBlock());
			} catch (Exception execute) {
				return PluginUtils.getObjectFromMainThread(() -> location.getBlock());
			}
		};
	}

	public static void setLocationResolver(LocationResolver resolver) {
		LOCATION_RESOLVER = resolver == null ? DefaultResolver.INSTANCE : resolver;
	}

	public static LocationResolver getLocationResolver() {
		return LOCATION_RESOLVER;
	}

	public static Block getBlock(Location location) {
		return BLOCK_DETECTOR.apply(location);
	}

	public static void locateWood(Location start, List<Location> current, IntCounter counter, int limit) {
		LOCATION_RESOLVER.resolve(start, CutterConfig.CHECK_RADIUS, current, counter, limit);
	}

	public static boolean isPlayerPlaced(Location location) {
		return LOCATION_RESOLVER.isPlayerPlaced(location);
	}
}
