package com.syntaxphoenix.spigot.smoothtimber.config;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import com.syntaxphoenix.spigot.smoothtimber.utilities.PluginUtils;

public final class ConfigTimer implements Runnable {

	public static final ConfigTimer TIMER = new ConfigTimer();

	private final ArrayList<STConfig> reload = new ArrayList<>();
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	private ConfigTimer() {

	}

	public boolean load(STConfig config) {
		WriteLock lock = this.lock.writeLock();
		lock.lock();
		boolean output;
		if (output = !reload.contains(config)) {
			reload.add(config);
		}
		lock.unlock();
		return output;
	}

	public boolean unload(STConfig config) {
		WriteLock lock = this.lock.writeLock();
		lock.lock();
		boolean output;
		if (output = reload.contains(config)) {
			reload.remove(config);
		}
		lock.unlock();
		return output;
	}

	@Override
	public void run() {
		ReadLock lock = this.lock.readLock();
		lock.lock();
		for (STConfig config : reload) {
			if (config.loaded < config.file.lastModified()) {
				String multiple = config.getMultipleType();
				PluginUtils.sendConsoleMessage(false, Message.GLOBAL_PREFIX.colored() + ' ' + Message.RELOAD_NEEDED.colored(new String[][] {
						{
								"%type0%",
								config.getSingleType()
						},
						{
								"%type1%",
								multiple
						}
				}));
				config.reload();
				PluginUtils.sendConsoleMessage(false, Message.GLOBAL_PREFIX.colored() + ' ' + Message.RELOAD_DONE.colored(new String[] {
						"%type%",
						multiple
				}));
			}
		}
	}

}
